Config = require('./config.js').Config;

var directoryMap = [
    {'dynamicAll': ''},
    {'cubiq': 'cubiq'},
    {'galleryPlayer': 'galleryPlayer'},
    {'mobileGallery': 'mobileGallery'},
    {'onet': 'onet'},
    {'onetNews': 'onetNews'},
    {'utils': 'utils'}
];

function generateConfForSingleDir(dir) {
  var conf = [];
  return {
    files: [{
              expand: true,
              cwd: Config.pathToScripts + '/' + dir,
              src: ['*.js', '!*.min.js'],
              dest: Config.pathToScripts + '/' + dir,
              ext: '.min.js'
          }]
  };
}

var uglifyConf = {
  options: {}
};

for (var i in directoryMap) {
  key = Object.keys(directoryMap[i])[0];
  conf = generateConfForSingleDir(directoryMap[i][key]);

  uglifyConf[key] = conf;
}

module.exports.UglifyConf = uglifyConf;