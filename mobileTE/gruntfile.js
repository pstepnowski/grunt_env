Config = require('./config.js').Config;
UglifyConf = require('./uglifyConf.js').UglifyConf;


module.exports = function(grunt) {
  var config = {
    pkg: grunt.file.readJSON('package.json'),
    uglify: UglifyConf
  };

  // Project configuration.
  grunt.initConfig(config);

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Default task(s).
  grunt.registerTask('default', ['uglify']);
};